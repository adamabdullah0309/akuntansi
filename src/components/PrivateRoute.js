// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React, {useState} from 'react'
// import AuthService from './Services/AuthService'
import { Redirect, Route } from 'react-router-dom'
import AuthService from './../service/AuthService'
import Session from '../Session/Session';
// import React from "react";


const PrivateRoute = ({ component: Component, ...rest }) => {

// //   // Add your own authentication on the below line.
// //   const isLoggedIn = AuthService.login({username : "adam", password : 123456});
// //   let data = "";
// //   var token = Session.getSession('token');
// //   state = {
// //     isLogged: false,
// //     }


// //   //ini untuk mendapatkan promise dari hasil service login yg kemudian dikasih state true or false ketika salah
// // //   Session.clearSession();
// // // console.log("token", Session.getSession('token'));
// // //   if(token == "" || token == null)
// // //   {
// //     isLoggedIn.then(function(data)
// //     {
// //         // return data=true;
// //         this.setState({ isLogged : true })
// //     })
// //     .catch(function(error)
// //     {
// //         return false;
// //     });

// //     // isLoggedIn.then((data2)=>
// //     // {
// //     //     console.log("data2",data2.data.result.token);
// //     //     // data = "true";
// //     //     data.reply = true;

    // })
    // .catch((data3)=>
    // {
    //     alert("login gagal");
    //     return;
    // });
//   }  
// const isLoggedIn = true;

const [logic, setLogic] = useState(false);
const [loading, setLoading] = useState(false);

AuthService.checkLogin({token : "Bearer "+Session.getSession('token')})
.then(function(data)
{
  // setLogic(logic = true);  
  setLogic(true);
  setLoading(true);
  console.log("data = ",data);
  console.log("berhasil = ",logic);

})
.catch(function(error)
{
  // console.log("gagal");
  setLogic(false);
  setLoading(true);
  console.log("gagal = ",logic);
  console.log(error);
});
console.log(logic);
  return (
    loading == true ? 
    <Route
      {...rest}
      render={props =>
        logic == true ? (
          
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
      }
    />
    : <div>Loading</div>
  )
}

export default PrivateRoute