import React, { Component } from 'react';
import { Layout, Form, Icon, Input, Button, Checkbox, Row, Col, Card, message } from 'antd';
import AuthService from './../service/AuthService'
import Session from '../Session/Session';

const { Content } = Layout;
const key = 'updatable';

class Login extends Component {
  state = {
		isLogged: false,
	}
  handleSubmit = e => {
    e.preventDefault();
    Session.clearSession();
		this.props.form.validateFields((err, values) => {
		  if (!err) {
      message.loading({ content: 'Loading...', key, duration: 0 });
      AuthService.login({username : values.username, password : values.password})
      .then(function(resp)
      {
        let data = resp.data;

        console.log(data.result);
        if(data.status === 200)
        {
          Session.setSession(data.result);
          message.success({ content: data.message, key, duration: 2 });
          // this.setState({ isLogged : true })
        }
        else
        {
          message.error({ content: 'Please check your username and password!', key, duration: 2 });
        }
        window.location.href = '/home';
        // console.log(data);
      })
      .catch(function(err)
      {
        if (err.response) {
          message.error({ content: err.response.data.message, key, duration: 2 });
        }else{
          message.error({ content: err.message, key, duration: 2 });
        }


      });
      
      }
    });
  }   

    // handleClick = e => {
    //     console.log('click ', e);
    //   };
    // state = {  }
    
    render() {
      const { isLogged } = this.state
	    const { getFieldDecorator } = this.props.form;

        return (
          <Content className="content-login">
          <Row className="row-login">
            <Col span={8} className="w-col-10"></Col>
            <Col span={8} className="w-col-80">
              <Card title="Form Login" bordered={false} className="card-login">
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <Form.Item>
                    {getFieldDecorator('username', {
                      rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                      <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Username"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: 'Please input your password!' }],
                    })(
                      <Input
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    {getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      initialValue: true,
                    })(<Checkbox>Remember me</Checkbox>)}
                    <a className="login-form-forgot" href="/">
                      Forgot password
                    </a>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Log in
                    </Button>
                    Or <a href="">register now!</a>
                  </Form.Item>
                </Form>
            </Card>
            </Col>
            <Col span={8} className="w-col-10"></Col>
          </Row>
          </Content>
  
        );
    }
}

const SignIn = Form.create({ name: 'normal_login' })(Login);
export default SignIn;