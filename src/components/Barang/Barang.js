import React from 'react';
import { Layout, Table, Button, Input, Icon, message, Tag, Modal } from 'antd';
import { Resizable } from 'react-resizable';
import Highlighter from 'react-highlight-words';

import BarangService from '../../service/BarangService';



import CreateModal from './CreateModal';
// import EditModal from './EditModal';

const { Content } = Layout;
const key = 'updatable';

const ResizeableTitle = props => {
  const { onResize, width, ...restProps } = props;

  if (!width) {
    return <th {...restProps} />;
  }

  return (
    <Resizable
      width={width}
      height={0}
      onResize={onResize}
      draggableOpts={{ enableUserSelectHack: false }}
    >
      <th {...restProps} />
    </Resizable>
  );
};

class Barang extends React.Component {

	constructor(props) {
    	super(props);
	    this.state = {
			searchText: '',
	    	searchedColumn: '',
		    columns: [
                {
                    title: 'Nama Barang',
                    dataIndex: 'barangNama',
                    key: 'barangNama',
                    sorter: (a, b) => a.barangNama.length - b.barangNama.length,
                    sortDirections: ['descend', 'ascend'],  
    
                },
                {
                    title: 'Status',
                    dataIndex: 'status',
                    key: 'status',
                },
                {
                    title: 'Action',
                    key: 'action',
                    render: (text, record) => 
                      this.state.data.length >= 1 ? (
                      <span>
                        <Button size="small" type="warning" onClick={this.showModal}>
                            Open Modal
                        </Button>
                        <Modal
                        title="Basic Modal"
                        visible={this.state.visible}
                        confirmLoading={this.state.confirmLoading}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        >
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                        </Modal>
                        {/* <EditModal reInit={this.getList} dataRecord={record} /> */}
                        {/*<Divider type="vertical" />
                          <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.customerId)}>
                            <Button size="small" type="danger" icon="delete">Delete</Button>
                          </Popconfirm>*/}
                      </span>
                      ) : null,
                  },
		    ],
		    data : []
		};
	}

	componentDidMount() {
		message.loading({ content: 'Loading...', key, duration: 0 });
		this.dataBarang();
	}

	dataBarang = () =>
    {
        BarangService.getListBarang({
            company_id :1
        })
        .then((resp) =>
        {
            let data = resp.data;

            let dataResp = [];
            for (var i = 0; i < data.responseData.length; i++) {
                let obj = data.responseData[i]
                dataResp.push({
                    key: i,
                    barangId: obj.barangId,
                    barangNama: obj.barangNama,
                    barangStatus: obj.barangStatus,
                    companyId: obj.companyId,
                    status: (obj.barangStatus == "y" ? <Tag color="green" key="active">
                        Active
                    </Tag> : <Tag color="volcano" key="inactive">
                        In Active
                    </Tag>),
                })
            }
            this.setState({data : dataResp});
            message.success({ content: 'Loaded!', key, duration: 2 });
        })
        .catch((err) =>
        {
			message.error({ content: err.message, key, duration: 2 });

        });
    }

	getColumnSearchProps = (dataIndex, title) => ({
	    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
	      <div style={{ padding: 8 }}>
	        <Input
	          ref={node => {
	            this.searchInput = node;
	          }}
	          placeholder={`Search ${title}`}
	          value={selectedKeys[0]}
	          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
	          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
	          style={{ width: 188, marginBottom: 8, display: 'block' }}
	        />
	        <Button
	          type="primary"
	          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
	          icon="search"
	          size="small"
	          style={{ width: 90, marginRight: 8 }}
	        >
	          Search
	        </Button>
	        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
	          Reset
	        </Button>
	      </div>
	    ),
	    filterIcon: filtered => (
	      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
	    ),
	    onFilter: (value, record) =>
	      record[dataIndex]
	        .toString()
	        .toLowerCase()
	        .includes(value.toLowerCase()),
	    onFilterDropdownVisibleChange: visible => {
	      if (visible) {
	        setTimeout(() => this.searchInput.select());
	      }
	    },
	    render: text =>
	      this.state.searchedColumn === dataIndex ? (
	        <Highlighter
	          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
	          searchWords={[this.state.searchText]}
	          autoEscape
	          textToHighlight={text.toString()}
	        />
	      ) : (
	        text
	      ),
	});

	handleSearch = (selectedKeys, confirm, dataIndex) => {
	    confirm();
	    this.setState({
	      searchText: selectedKeys[0],
	      searchedColumn: dataIndex,
	    });
	};

	handleReset = clearFilters => {
	    clearFilters();
	    this.setState({ searchText: '' });
	};

	handleDelete = id => {
		const dataSource = [...this.state.data];
		this.setState({ data: dataSource.filter(item => item.customerId !== id) });
		message.success({ content: 'Success delete data!', duration: 2 });
	};

	components = {
		header: {
		  cell: ResizeableTitle,
		},
	};

	handleResize = index => (e, { size }) => {
		this.setState(({ columns }) => {
		  const nextColumns = [...columns];
		  nextColumns[index] = {
		    ...nextColumns[index],
		    width: size.width,
		  };
		  return { columns: nextColumns };
		});
	};

	render() {
		const columns = this.state.columns.map((col, index) => {
            console.log("log = ",col);
			if (col.dataIndex && col.dataIndex !== "barangNama" && col.dataIndex !== "barangStatus") {
				return {
			      ...col,
			      ...this.getColumnSearchProps(col.dataIndex, col.title),
			      onHeaderCell: column => ({
			        width: column.width,
			        onResize: this.handleResize(index),
			      }),
			    }
			}else{
				return {
			      ...col,
			      onHeaderCell: column => ({
			        width: column.width,
			        onResize: this.handleResize(index),
			      }),
			    }
			}
	    });

		return (
			<Content
	            style={{
	              margin: '24px 16px',
	              padding: 24,
	              background: '#fff',
	              minHeight: 280,
	            }}
	        >
	        	<CreateModal reInit={this.dataBarang}/>
				<Table bordered components={this.components} columns={columns} dataSource={this.state.data} size="middle" />
	        </Content>
		);
	}
}

export default Barang;
