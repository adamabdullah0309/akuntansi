import React from 'react';
import { Button, Modal, Form, Input, message, Row, Col, InputNumber, Select } from 'antd';

// import EmployeeService from './Service/EmployeeService';
// import PositionEmployeeService from './Service/PositionEmployeeService';
// import DivisionService from './Service/DivisionService';
import Session from './../../Session/Session';

const { TextArea } = Input;
const { Option } = Select;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {

    state = {
      dataPosition: [],
      dataDivision: [],
    }
    
    componentDidMount() {
    //   this.getPosition()
    //   this.getDivision()
    }

   

    render() {
      const { visible, onCancel, onCreate, form, confirmLoading } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Create a employee"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
          confirmLoading={confirmLoading}
        >
          <Form layout="vertical">
            <Row gutter={16}>
              <Col className="gutter-row" span={12}>
                <div className="gutter-box">
                  <Form.Item label="Employee Name">
                    {getFieldDecorator('employeeName', {
                      rules: [{ required: true, message: 'Please input the employee name!' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Employee Phone">
                    {getFieldDecorator('employeePhone', {
                      rules: [{ required: true, message: 'Please input the employee phone!' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Employee Email">
                    {getFieldDecorator('employeeEmail', {
                      rules: [{ required: true, message: 'Please input the employee email!' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Employee Address">
                    {getFieldDecorator('employeeAddress', {
                      rules: [{ required: true, message: 'Please input the employee address!' }],
                    })(<TextArea placeholder="Address" allowClear />)}
                  </Form.Item>
                </div>
              </Col>
              
            </Row>
          </Form>
        </Modal>
      );
    }
  },
);

class CreateModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      visible: false,
      confirmLoading: false,
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      this.setState({
        confirmLoading: true,
      });

      

    //   EmployeeService.saveEmployee({
    //     company_id: Session.getSession('companyId'),
    //     user: Session.getSession('username'),
    //     nmkaryawan: values.employeeName,
    //     handphone: values.employeePhone,
    //     email: values.employeeEmail,
    //     alamat: values.employeeAddress,
    //     jabatan: values.employeePosition,
    //     divisi: values.employeeDivision,
    //     targetsales: values.employeeTargetSales,
    //   }).then((resp) => {
    //     let data = resp.data
    //     if (data.responseCode === "00") {
    //       form.resetFields();
    //       message.success({ content: data.responseDesc, duration: 2 });
    //       this.props.reInit()
    //     }else{
    //       message.error({ content: data.responseDesc, duration: 2 });
    //     }
    //     this.setState({ 
    //       visible: false,
    //       confirmLoading: false,
    //     });
    //   }).catch((e) => {
    //     this.setState({ 
    //       visible: false,
    //       confirmLoading: false,
    //     });
    //     message.error({ content: e.message, duration: 2 });
    //   })
    });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal} style={{ marginBottom: 16, display: 'flex',
  justifyContent : 'left' }} icon="plus">
          Create New
        </Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          confirmLoading={this.state.confirmLoading}
        />
      </div>
    );
  }
}

export default CreateModal