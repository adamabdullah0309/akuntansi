import React from 'react';
import { BrowserRouter as Router, Route, Redirect,Switch } from 'react-router-dom';
import NotFoundPage from './components/NotFoundPage';
import Login from './components/Login';
import PrivateRoute from './components/PrivateRoute';
import Layout2  from './components/Layout/Layout';

function routes() {
    return(
        <Router>
                <Switch>
                    <PrivateRoute exact path="/home" component={Layout2} />
                    <Route path="/Login" component={Login} />
                    
                    <PrivateRoute component={Layout2} />
                </Switch>
        </Router>
    );
}

export default routes